﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LolPreAlpha1.Models.ViewModels;
using LolPreAlpha1.Bussines;

namespace LolPreAlpha1.Controllers
{
    public class MemberController : Controller
    {
        //
        // GET: /Member/

        public ActionResult Index()
        {
            return View();
        }

         public ActionResult CreateQue()
        {
            if (Session["User"] != null)
            {
                QueService service = new QueService();
                QueViewModel model = service.getmodel();
                return View(model);
            }
            return RedirectToAction("Login", "Account");
        }

         [HttpPost]
         public ActionResult CreateQue(QueViewModel model)
         {
             if (ModelState.IsValid)
             {
                 QueService service = new QueService();
                 if (service.CreateQueInstance(model) == true)
                 {
                     return RedirectToAction("Index", "Home");
                 }
                 else
                     return RedirectToAction("CreateQue", "Member");
             }
             else
             {
                 AccountServices service = new AccountServices();
                 service.ClearRegisterView();//??NEED CLEARQUEVİEW!!
             }
             return View(model);
             
         }


    }
}
