﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LolPreAlpha1.Models;
using LolPreAlpha1.Models.ViewModels;
using LolPreAlpha1.Bussines;

namespace LolPreAlpha1.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register(AccountViewModels.RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                AccountServices service = new AccountServices();                 
                if(service.CreateUserInstance(model) == true)
                { 
                return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.message = "Bu kullanıcı adında tr sunucusunda bir sihirdar yok, Lütfen geçerli bir sihirdar adı giriniz..";
                    service.ClearRegisterView();
                }
            }

            else
            {
                AccountServices service = new AccountServices();
                service.ClearRegisterView();
            }
            return View(model);
        }

        public ActionResult ActivateAccount(string PasswordHash)
        {
            AccountServices service = new AccountServices();

            if (service.ActivateAccountResult(PasswordHash) == true)
            {
                return RedirectToAction("Login");
            }
            return RedirectToAction("Index");
        }


        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(AccountViewModels.LoginViewModel model)
        {
            if(ModelState.IsValid)
            {
                AccountServices service = new AccountServices();
                service.LoginTry(model);
                if(Session["User"] != null)
                {
                    RiotApiService apiService = new RiotApiService();
                    apiService.AfterLoginApiCallService();                   
                    Session.Timeout = 10;
                    return RedirectToAction("Index","Home");
                }

            }
            return RedirectToAction("Login");
        }

        public ActionResult PasswordChange()
        {
            if (Session["User"] == null)
            {
                return RedirectToAction("Login");
            }
            return View();
        }

        [HttpPost]
        public ActionResult PasswordChange(AccountViewModels.ManageUserViewModel model)
        {
            AccountServices service = new AccountServices();
            service.PasswordChangeService(model);
            return RedirectToAction("Login");
        }

        public ActionResult LogOut()
        {
            Session["User"] = null;
            Session["League"] = null;            
            return RedirectToAction("Index",
                        "Home");
        }
    }
}
