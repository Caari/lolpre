﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LolPreAlpha1.Models.ViewModels
{
    public class RiotApiViewModel2
    {
        [JsonProperty("entries")]
        public List<LeagueEntryDto> entries { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("participantId")]
        public string participantId { get; set; }
        [JsonProperty("queue")]
        public string queue { get; set; }
        [JsonProperty("tier")]
        public string tier { get; set; }


        public class LeagueEntryDto
        {
            [JsonProperty("division")]
            public string division { get; set; }
            [JsonProperty("isFreshBlood")]
            public Boolean isFreshBlood { get; set; }
            [JsonProperty("isHotStreak")]
            public Boolean isHotStreak { get; set; }
            [JsonProperty("isInactive")]
            public Boolean isInactive { get; set; }
            [JsonProperty("isVeteran")]
            public Boolean isVeteran { get; set; }
            [JsonProperty("leaguePoints")]
            public int leaguePoints { get; set; }
            [JsonProperty("losses")]
            public int losses { get; set; }
            [JsonProperty("miniSeries")]
            public MiniSeriesDto miniSeries { get; set; }
            [JsonProperty("playerOrTeamId")]
            public string playerOrTeamId { get; set; }
            [JsonProperty("playerOrTeamName")]
            public string playerOrTeamName { get; set; }
            [JsonProperty("wins")]
            public int wins { get; set; }

        }
        public class MiniSeriesDto
        {
            [JsonProperty("losses")]
            public int losses { get; set; }
            [JsonProperty("progress")]
            public string progress { get; set; }
            [JsonProperty("target")]
            public int target { get; set; }
            [JsonProperty("wins")]
            public int wins { get; set; }
        }
    }
}