﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LolPreAlpha1.Models.ViewModels
{
    public class QueViewModel
    {
        public int LFLaneID { get; set; }
        public int MyLaneID { get; set; }
        public int VoiceID { get; set; }
        public List<SelectListItem> LfRoleList { get; set; }
        public List<SelectListItem> MyRoleList { get; set; }
        public List<SelectListItem> VoiceList { get; set; }

        public System.DateTime PostTime { get; set; }

        public string UserMessage { get; set; }
    }
}