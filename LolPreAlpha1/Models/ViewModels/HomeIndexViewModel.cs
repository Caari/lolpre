﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LolPreAlpha1.Models.ViewModels
{
    public class HomeIndexViewModel
    {
        public List<LolPreAlpha1.Models.Que> QueList { get; set; }
        public string UserName { get; set; }
        public List<string> BannerUrlList { get; set; }
    }
}