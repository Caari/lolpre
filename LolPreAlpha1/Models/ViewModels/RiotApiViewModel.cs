﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LolPreAlpha1.Models.ViewModels
{
    public class RiotApiViewModel
    {
        [JsonProperty("id")]
        public long id { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("profileIconId")]
        public int profileIconId { get; set; }
        [JsonProperty("revisionDate")]
        public long revisionDate { get; set; }
        [JsonProperty("summonerLevel")]
        public long summonerLevel { get; set; }
    }
}