﻿using LolPreAlpha1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LolPreAlpha1.Models.ViewModels;
using System.Web.Mvc;

namespace LolPreAlpha1.Bussines
{
    public class QueManagementDao
    {
        public Que newQue { get; set; }
        public User User { get; set; }

        LolPreDb2Entities db = new LolPreDb2Entities();

        public void AddQueToDb(Que newQue)
        {
            User _tUser = new User();
            _tUser = (User)HttpContext.Current.Session["User"];
            newQue.UserID = _tUser.UserID;            
            db.Que.Add(newQue);
            db.SaveChanges();
        }

        public List<Que> MainPageQueLister()
        {
            List<Que> quelist = db.Que.ToList();
            return quelist;
        }




        public QueViewModel getModel()
        {
            QueViewModel model = new QueViewModel();

                model.LfRoleList = (from rol in db.Lane.ToList()
                                    select new SelectListItem
                                    {
                                        Selected = false,
                                        Text = rol.LaneName,
                                        Value = rol.LaneID.ToString()

                                    }).ToList();
                model.LfRoleList.Insert(0, new SelectListItem
                {
                    Selected = true,
                    Value = "",
                    Text = "--Seçiniz--"
                });


                model.MyRoleList = (from rol2 in db.Lane.ToList()
                                    select new SelectListItem
                                    {
                                        Selected = false,
                                        Text = rol2.LaneName,
                                        Value = rol2.LaneID.ToString()
                                    }).ToList();
                model.MyRoleList.Insert(0, new SelectListItem
                {
                    Selected = true,
                    Value = "",
                    Text = "--Seçiniz--"
                });

                model.VoiceList = (from vol in db.Voice.ToList()
                                   select new SelectListItem
                                   {
                                       Selected = false,
                                       Text = vol.VoiceChoice,
                                       Value = vol.VoiceID.ToString()
                                   }).ToList();

                model.VoiceList.Insert(0, new SelectListItem
                {
                    Selected = true,
                    Value = "",
                    Text = "--Seçiniz--"
                });
                return model;
            
                
        }
    }
}