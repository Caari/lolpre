﻿using LolPreAlpha1.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Hosting;


namespace LolPreAlpha1.Bussines
{
    public class AccountManagementDao
    {
        public User newUser { get; set; }        
        LolPreDb2Entities db = new LolPreDb2Entities();

        //Account Add
        public void AddUserToDb(User newUser)
        {            
            db.User.Add(newUser);
            db.SaveChanges();
        }       
        //Send Activate Account Email
        public void SendEmail(User newUser)        
        {            
            var message = EmailTemplate("EmailPage");
            message = message.Replace("@ViewBag.Name", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(newUser.UserName));
            message = message.Replace("@ViewBag.Link", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(newUser.PasswordHash));
            EmailSender(newUser.Email, "Hoş Geldin!", message);           
        }
        //Email Sender
        public void EmailSender(string email, string subject, string message)
       {
           var _email = "lolpreservices@gmail.com";
           var _pass = ConfigurationManager.AppSettings["EmailPassword"];
           var _dispName = "LolPre Activation Email";
           MailMessage mail = new MailMessage();
           mail.To.Add(email);
           mail.From = new MailAddress(_email, _dispName);
           mail.Subject = subject;
           mail.Body = message;
           mail.IsBodyHtml = true;

            using (SmtpClient smtp = new SmtpClient())
            {
                smtp.EnableSsl = true;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(_email, _pass);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;                
                smtp.Send(mail);
                smtp.SendCompleted += (s, e) => { smtp.Dispose(); };
            }

        }

        //Email Template
        public string EmailTemplate(string template)
        {
            var templateFilePath = HostingEnvironment.MapPath("~/Content/templates/") + template + ".cshtml";
            StreamReader objstreamreaderfile = new StreamReader(templateFilePath);
            var body = objstreamreaderfile.ReadToEnd();
            objstreamreaderfile.Close();
            return body;

        }
        //Account Activation Modify
        public bool ActivateAccountDa(string PasswordHash)
        {
            User newUser = new User();
            newUser = db.User.SingleOrDefault(u => u.PasswordHash == PasswordHash);
            if (newUser != null)
            {
                newUser.EmailConfirmation = true;
                db.Entry(newUser).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            else
                return false;
        }
        //LOGIN 
        public User Login(string Email,string Password)
        {
            User AttemptUser = new User();
            AttemptUser = db.User.SingleOrDefault(u => u.Email == Email && u.Password == Password);
            if(AttemptUser != null)
            {
                if(AttemptUser.UserRole != 2)
                {
                    if (AttemptUser.EmailConfirmation == true)
                    {
                        return AttemptUser;
                    }
                    else { 
                        return null;
                    }   
                }
                else
                {
                    return null;
                }  
            }
            else
            {
                return null;
            }  
        }

        //PasswordChange
        public void PasswordChangeDa(string Email,string Password)
        {
            if(HttpContext.Current.Session["User"] != null)
            { 
            User _tUser = new User();
            _tUser = db.User.SingleOrDefault(u => u.Email == Email);
            _tUser.Password = Password;
            db.Entry(_tUser).State = EntityState.Modified;
            db.SaveChanges();
            HttpContext.Current.Session["User"] = null;
            }
            HttpContext.Current.Session["User"] = null;
        }

        

    }
}