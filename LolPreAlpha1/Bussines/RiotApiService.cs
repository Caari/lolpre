﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net;
using LolPreAlpha1.Models;
using LolPreAlpha1.Models.ViewModels;
using Newtonsoft.Json;

namespace LolPreAlpha1.Bussines
{
    public class RiotApiService
    {
        public User User { get; set; }


        public bool CreateAccountValidation(string UserName)
        {
            string apiUrl = String.Format("https://tr.api.pvp.net/api/lol/tr/v1.4/summoner/by-name/{0}?api_key=6e5993dd-1ac7-477e-b634-713535f39c9e", UserName);    
            WebClient client = new WebClient();
            try
            {
                if (client.DownloadString(apiUrl.ToString()) != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            } 
        }



        //League Info Call By RiotApi
        public void AfterLoginApiCallService()
        {
            if (HttpContext.Current.Session["User"] != null)
            {
                User _tUser = new User();
                HttpContext.Current.Session["League"] = null;
                _tUser = (User)HttpContext.Current.Session["User"];

                //Creat ApiURL and take json_response And then making 2nd call for tier and division and sending em into session
                string apiUrl = String.Format("https://tr.api.pvp.net/api/lol/tr/v1.4/summoner/by-name/{0}?api_key=6e5993dd-1ac7-477e-b634-713535f39c9e", _tUser.UserName);
                WebClient client = new WebClient();
                if(client.DownloadString(apiUrl.ToString()) == null )
                { 
                    //error cevırebılırım bu ısımde lol kullanıcısı yok şeklinde
                    HttpContext.Current.Session["League"] = null;
                }
                else {
                string json_response = client.DownloadString(apiUrl.ToString());                                
                Dictionary<string, RiotApiViewModel> foundSummoner = JsonConvert.DeserializeObject<Dictionary<string, RiotApiViewModel>>(json_response);                
                long id = 0;
                foreach (var item in foundSummoner)
                {
                    id = item.Value.id;
                }
                string apiUrl2 = String.Format("https://tr.api.pvp.net/api/lol/tr/v2.5/league/by-summoner/{0}/entry?api_key=6e5993dd-1ac7-477e-b634-713535f39c9e", id.ToString());

                WebClient client2 = new WebClient();                    
                string json_response2 = client2.DownloadString(apiUrl2.ToString());
                Dictionary<string, List<RiotApiViewModel2>> foundSummoner2 = JsonConvert.DeserializeObject<Dictionary<string, List<RiotApiViewModel2>>>(json_response2);

                string tier = "";
                string division = "";
                foundSummoner2.Cast<object>().ToArray();
                foreach (var item in foundSummoner2)
                {
                    tier = item.Value[0].tier;
                    division = item.Value[0].entries[0].division;
                }
                HttpContext.Current.Session["League"] = tier + " " + division;
                
                
                }
            }
            else
            {
                HttpContext.Current.Session["League"] = null;
            }
        }
    }
}