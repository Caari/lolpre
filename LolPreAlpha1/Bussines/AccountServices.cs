﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LolPreAlpha1.Models;
using LolPreAlpha1.Models.ViewModels;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;
using System.IO;
using System.Web.Hosting;



namespace LolPreAlpha1.Bussines
{
    
    public class AccountServices
    {
        public AccountManagementDao dao { get; set; }
        public User User { get; set; }
        public RiotApiService ApiService { get; set; }
        
        AccountManagementDao _dao = new AccountManagementDao();
        RiotApiService apiservice = new RiotApiService();

        //CREATE ACCOUNT
        public bool CreateUserInstance(AccountViewModels.RegisterViewModel model)
        {

            if (apiservice.CreateAccountValidation(model.UserName) == true)
            {
                string UserHashKey = GetUserHashKey(model.Password).ToString();
                User _tUser = new User(model.UserName, model.Email, model.Password, UserHashKey);
                _dao.AddUserToDb(_tUser);
                _dao.SendEmail(_tUser);
                return true;
            }
            else
                return false;
                
        }

        //PASSWORD HASHER
        public string GetUserHashKey(string _password)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            Byte[] originalBytes = ASCIIEncoding.Default.GetBytes(_password);
            Byte[] encodedBytes = md5.ComputeHash(originalBytes);
            string hashkey = BitConverter.ToString(encodedBytes).Replace("-", "").ToLower();
            return hashkey;
        }
        //CLEAR REGISTER
        public AccountViewModels.RegisterViewModel ClearRegisterView()
        {
            AccountViewModels.RegisterViewModel viewModel = new AccountViewModels.RegisterViewModel();            
            viewModel.UserName = "";
            viewModel.Email = "";
            viewModel.Password = "";
            viewModel.ConfirmPassword = "";
            return viewModel;
        }        

        public bool ActivateAccountResult(string PasswordHash)
        {             
           bool result = _dao.ActivateAccountDa(PasswordHash);
           return result;
        }
        //LOGIN SERVICE  
        public void LoginTry(AccountViewModels.LoginViewModel model)
        {
            User _tUser = _dao.Login(model.Email,model.Password);
           if(  _tUser != null)
           {  
               HttpContext.Current.Session["User"] = _tUser;
           }
           else
           {
               HttpContext.Current.Session["User"] = null;
           }
        }

        public void PasswordChangeService(AccountViewModels.ManageUserViewModel model)
        {
            User _tUser = new User();
            _tUser = (User)HttpContext.Current.Session["User"];
            _dao.PasswordChangeDa(_tUser.Email.ToString(),model.NewPassword);
            
        }
    }
}