﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LolPreAlpha1.Models;
using LolPreAlpha1.Models.ViewModels;

namespace LolPreAlpha1.Bussines
{
    public class QueService
    {
        public QueManagementDao dao { get; set; }
        public User User { get; set; }

        QueManagementDao _dao = new QueManagementDao();

        public bool CreateQueInstance(QueViewModel model)
        {
            if (HttpContext.Current.Session["User"] != null)
            {
                string league = HttpContext.Current.Session["League"].ToString();
                User _tUser = new User();
                _tUser = (User)HttpContext.Current.Session["User"];
                int UserId = _tUser.UserID;
                Que _tQue = new Que(UserId, league, model.LFLaneID, model.MyLaneID, model.VoiceID);
                _dao.AddQueToDb(_tQue);
                return true;
            }
            else
                return false;             
        }
        public QueViewModel getmodel()
        {
            if(HttpContext.Current.Session["User"] != null)
            {                 
                return _dao.getModel();
            }
            return _dao.getModel();
        }


        public HomeIndexViewModel MainPageQueService()
        {
            var QueList = _dao.MainPageQueLister();
            HomeIndexViewModel model = new HomeIndexViewModel();
            model.QueList = QueList;
            return model;
        }


    }
}